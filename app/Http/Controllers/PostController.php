<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// access the authenticated user via the Auth Facades
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    // action to return a view containing a form for a blog post creation.
    public function create(){
        return view('posts.create');
    }

    // action to receive form data and subsequently store said data in the posts table.
    
    public function store(Request $request){
        if(Auth::user()){
            $post = new Post;
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = (Auth::user()->id);

            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('login');
        }
    }

    // action that will return a view showing all blog posts.
    public function index(){
        $posts = Post::all();
        return view('posts.index')->with('posts', $posts);
    }

    public function welcome(){
        $posts = Post::all();
        return view('welcome')->with('posts', $posts->random(3));
    }
}
