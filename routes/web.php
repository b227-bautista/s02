<?php

use Illuminate\Support\Facades\Route;
//link the PostController fil
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/about', function () {
    return view('about');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//define a route wherein a view to create a post that will be returned to the user (form for creating a post)
Route::get('/posts/create',[PostController::class, 'create']);

//define a route wherein form data will be sent via POST method to the /post URI endpoint.
Route::post("/posts", [PostController::class, 'store']);

// define a route that will return a view containing all the posts.
Route::get('/posts', [PostController::class, 'index']);

Route::get('/', [PostController::class, 'welcome']);